Name: btrfsmaintenance
Summary: Automatic maintenance of BTRFS filesystems
Version: 0.4.2
Release: alt1
Packager: Mikhail Novosyolov <mikhailnov@altlinux.org>
License: GPL
Group: System/Base
# Real upstream: https://github.com/kdave/btrfsmaintenance
# Bellow is a fork with some adjustments
Url: https://gitlab.com/nixtux-packaging/btrfsmaintenance
BuildArch: noarch
Source0: %name-%version.tar
Patch0: ALT-remove-etc-default.patch

BuildRequires: pkgconfig(systemd)
# https://bugzilla.altlinux.org/35388
BuildRequires: rpm-macros-fedora-compat
Requires: btrfs-progs

%description
This is a set of scripts supplementing the btrfs filesystem and aims to automate
a few maintenance tasks. This means the scrub, balance, trim or defragmentation.

Each of the tasks can be turned on/off and configured independently.
Overall tuning of the default values should give a good balance between
effects of the tasks and low impact of other work on the system. 
If this does not fit your needs, please adjust the settings.

systemd units (services, timers, path watchers) are enabled automatically.
If you want to run scripts via cron, configure it yourself (see documentation:
https://github.com/kdave/btrfsmaintenance/blob/master/README.md)

Config file is %{_sysconfdir}/sysconfig/btrfsmaintenance
Maintenance timers are enabled by default.

%prep
%setup -q
%patch0 -p1

%build

%install
# scripts
install -m 755 -d %{buildroot}%{_datadir}/%{name}
install -m 755 btrfs-defrag.sh %{buildroot}%{_datadir}/%{name}
install -m 755 btrfs-balance.sh %{buildroot}%{_datadir}/%{name}
install -m 755 btrfs-scrub.sh %{buildroot}%{_datadir}/%{name}
install -m 755 btrfs-trim.sh %{buildroot}%{_datadir}/%{name}
install -m 755 btrfsmaintenance-refresh-cron.sh %{buildroot}%{_datadir}/%{name}
install -m 644 btrfsmaintenance-functions %{buildroot}%{_datadir}/%{name}

# systemd services and timers
install -m 755 -d %{buildroot}%{_unitdir}
install -m 755 -d %{buildroot}%{_presetdir}
install -m 644 -D btrfsmaintenance-refresh.service %{buildroot}%{_unitdir}/
install -m 644 -D btrfsmaintenance-refresh.path %{buildroot}%{_unitdir}/
install -m 644 -D btrfs-balance.service %{buildroot}%{_unitdir}/
install -m 644 -D btrfs-defrag.service %{buildroot}%{_unitdir}/
install -m 644 -D btrfs-scrub.service %{buildroot}%{_unitdir}/
install -m 644 -D btrfs-trim.service %{buildroot}%{_unitdir}/
install -m 644 -D btrfs-balance.timer %{buildroot}%{_unitdir}/
install -m 644 -D btrfs-defrag.timer %{buildroot}%{_unitdir}/
install -m 644 -D btrfs-scrub.timer %{buildroot}%{_unitdir}/
install -m 644 -D btrfs-trim.timer %{buildroot}%{_unitdir}/
install -m 644 -D 80-btrfsmaintenance.preset %{buildroot}%{_presetdir}/

# config
install -m 644 -D sysconfig.btrfsmaintenance %{buildroot}%{_sysconfdir}/sysconfig/btrfsmaintenance

%post
# According to 80-btrfmaintenance.preset,
# needed systemd units will be enabled automatically on package installation
%systemd_post btrfsmaintenance-refresh.service btrfsmaintenance-refresh.path btrfs-balance.service btrfs-balance.timer btrfs-defrag.service btrfs-defrag.timer btrfs-scrub.service btrfs-scrub.timer btrfs-trim.service btrfs-trim.timer

%preun
%systemd_preun btrfsmaintenance-refresh.service btrfsmaintenance-refresh.path btrfs-balance.service btrfs-balance.timer btrfs-defrag.service btrfs-defrag.timer btrfs-scrub.service btrfs-scrub.timer btrfs-trim.service btrfs-trim.timer

%files
%doc COPYING README.md
%config(noreplace) %{_sysconfdir}/sysconfig/btrfsmaintenance
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/*
%{_unitdir}/btrfs*
%{_presetdir}/*btrfs*

%changelog
* Fri May 31 2019 Mikhail Novosyolov <mikhailnov@altlinux.org> 0.4.2-alt1
- Initial build for ALT Linux Sisyphus
