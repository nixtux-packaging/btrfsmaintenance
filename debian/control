Source: btrfsmaintenance
Section: admin
Priority: optional
Maintainer: Mikhail Novosyolov <mikhailnov@dumalogiya.ru>
Build-Depends: debhelper (>= 9), dh-exec, dh-systemd
Standards-Version: 4.1.3
Homepage: https://github.com/kdave/btrfsmaintenance
Vcs-Git: https://salsa.debian.org/sten-guest/btrfsmaintenance.git
Vcs-Browser: https://salsa.debian.org/sten-guest/btrfsmaintenance

Package: btrfsmaintenance
Architecture: all
Depends: ${misc:Depends},
         btrfs-progs | btrfs-tools,
         systemd
Enhances: btrfs-progs, btrfs-tools
Description: automate btrfs maintenance tasks on mountpoints or directories
 This is a set of scripts for the btrfs filesystem that automates the
 following maintenance tasks: scrub, balance, trim, and defragment.
 .
 Tasks are enabled, disabled, scheduled, and customised from a single
 text file.  The default configuration assumes an installation profile
 where / is a btrfs filesystem.
 .
 The default values have been chosen as an even compromise between
 time to complete maintenance, improvement in filesystem performance,
 and minimum impact on other processes.  Please note that I/O priority
 scheduling requires the use of CFQ, and not noop, deadline,
 anticipatory, or blk-mq.  CFQ is Debian's default block scheduler.
