# Debian packaging of btrfsmaintenance

Based on upstream Debian's package (https://packages.debian.org/buster/btrfsmaintenance). Changes compared to Debian's package:

* remove all dh_systemd overrides in `debian/rules`
* add dh --with systemd to `debian/rules`
* drop compat from 11 to 9 (to allow building on Ubuntu 16.04) (`debian/compat`)
* All systemd units are enabled automatically
* Set epoch 2 to disallow package from Ubuntu repositories to override mine
* remove `debian/source`
* Fixed build dependencies in `debian/control`: debhelper >= 9 instead of >= 11, remove version dependency of dh-exec, build-depend from dh-systemd for building on Ubuntu 16.04 xenial
* Fix runtime dependency: change `btrfs-progs` to `btrfs-progs | btrfs-tools` to support older Debian and Ubuntu releases, including Ubuntu 16.04, where this package had another name
* Fixed commas in the list of runtime dependencies, removed cron
* Что-то не понял, в debian/README.debian и debian/rules утверждается, что униты (таймеры) systemd не должны автоматически включаться, но вроде бы  в скриптах собранного пакета они включались. Мне нужно, чтобы они всегда включались, бюррократы чертовы дебиановские.

The code of btrfsmaintenance is not modified and will be synced with upstream from time to time.

Also, see debian/changelog for details.
